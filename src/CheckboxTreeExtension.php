<?php

namespace Xn\CheckboxTree;

use Xn\Admin\Extension;

class CheckboxTreeExtension extends Extension
{
    public $name = "laravel-admin-checkboxtree";

    public $views = __DIR__.'/../resources/views';

    public $assets = __DIR__.'/../resources/assets';
}
