<?php

namespace Xn\CheckboxTree;

use Xn\Admin\Form\Field\MultipleSelect;
use Xn\CheckboxTree\Traits\CheckboxTreeBuilder;

class CheckboxTree extends MultipleSelect
{

    use CheckboxTreeBuilder;

    protected $text = "name";

    protected $key = "id";

    protected $slug = "slug";

    protected $treeOptions = [];

    protected $buildRoot = false;

    protected $view = 'laravel-admin-checkboxtree::checkboxtree';

    protected static $css = [
        '/vendor/laravel-admin-ext/checkboxtree/bootree/dist/bootree.css'
    ];

    protected static $js = [
        '/vendor/laravel-admin-ext/checkboxtree/bootree/dist/bootree.js'
    ];

    protected $pattern = "\.[^.]+$/";


    /**
     * Checkbox Regex
     *
     * @param string $pattern \.[^.]+$/
     * @return $this
     */
    public function cbPattern(string $pattern = "\.[^.]+$/")
    {
        $this->pattern = $pattern;
        return $this;
    }

    /**
     * 選項
     *
     * @param $options
     * @return void
     */
    public function treeOptions($options, $buildRoot = false){
        $this->treeOptions = $options->toArray();
        $this->buildRoot = $buildRoot;
        return $this;
    }

    /**
     * 子選單
     *
     * @param [type] $group_key
     * @param array $options
     * @return void
     */
    protected function optionChildren($parentOpt = [], $treeOptions = [], $checked = []) {
        $options = collect($treeOptions)->filter(function ($option) use($parentOpt) {
            try {
                $pattern = "/^{$parentOpt[$this->slug]}" . $this->pattern;
                preg_match($pattern, $option[$this->slug], $matches, PREG_OFFSET_CAPTURE);
                return count($matches)>0;
            } catch (\Throwable $th) {
                return false;
            }
        })->toArray();

        foreach($options as $option) {
            $label = $option[$this->text];
            $key = $option[$this->key];
            $slug = $option[$this->slug];

            $opt = [
                'id' => $key,
                'text' => $label,
                'slug' => $slug,
                'checked' => in_array(intval($key), $checked),
                'children' => []
            ];

            $childOpts = collect($treeOptions)->filter(function ($option) use ($slug) {
                try {
                    $pattern = "/^{$slug}" . $this->pattern;
                    preg_match($pattern, $option[$this->slug], $matches, PREG_OFFSET_CAPTURE);
                    return count($matches)>0;
                } catch (\Throwable $th) {
                    return false;
                }
            })->toArray();

            if (count($childOpts)) {
                $opt = $this->optionChildren($opt, $treeOptions, $checked);
            }

            array_push($parentOpt['children'], $opt);
        }

        return $parentOpt;
    }

    protected function treeData(&$treedata = [], $treeOptions = [], $checked = []) {

        $options = collect($treeOptions)->filter(function ($option) {
            try {
                return false === stripos($option[$this->slug], ".");
            } catch (\Throwable $th) {
                return false;
            }
        })->toArray();

        foreach($options as $option) {
            $label = $option[$this->text];
            $key = $option[$this->key];
            $slug = $option[$this->slug];

            $opt = [
                'id' => $key,
                'text' => $label,
                'slug' => $slug,
                'checked' => in_array(intval($key), $checked),
                'children' => []
            ];

            $childOpts = collect($treeOptions)->filter(function ($option) use ($slug) {
                try {
                    $pattern = "/^{$slug}" . $this->pattern;
                    preg_match($pattern, $option[$this->slug], $matches, PREG_OFFSET_CAPTURE);
                    return count($matches)>0;
                } catch (\Throwable $th) {
                    return false;
                }
            })->toArray();

            if (count($childOpts)) {
                $opt = $this->optionChildren($opt, $treeOptions, $checked);
            }

            array_push($treedata, $opt);
        }
    }

    /**
     * 建立根節點
     *
     * @param array $data
     */
    private function buildRoot(array &$data)
    {
        foreach($data as &$tree){
            if (count($tree['children']??[])>0) {
                $home = $tree;
                $tree['id'] = intval($tree['id'])+10000;
                unset($home['children']);
                $home['text'] = __('root');
                $tree['checked'] = false;
                array_unshift($tree['children'], $home);
                $this->buildRoot($tree['children']);
            }
        }
    }

    public function render()
    {
        $this->addVariables([
            'options' => $this->options,
        ]);

        $treedata = [];
        $treeOptions = $this->treeOptions;
        $checked = $this->value()??[];

        $this->treeData($treedata, $treeOptions, $checked);

        if ($this->buildRoot) {
            $this->buildRoot($treedata);
        }

        $data = json_encode($treedata);
        $this->script = <<<EOT
var myData = $data;
const tree{$this->id} = $('#{$this->id}_tree').tree({
    primaryKey: 'id',
    dataSource: myData,
    checkboxes: true
});

tree{$this->id}.change(function(e){
    var elements = document.getElementById("{$this->id}").options;
    for(var i = 0; i < elements.length; i++){
        elements[i].selected = false;
    }
    tree{$this->id}.getCheckedNodes().forEach(function(selIdx){
        try {
            $('select[name="{$this->id}[]"]').find("[value="+selIdx+"]")[0].selected = 'selected';
        } catch (e) {
            console.log(e); // Logs the error
        }
    });
});

EOT;
        return parent::render();
    }
}
